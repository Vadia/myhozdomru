
<html lang="ru">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Animate CSS --> 
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="assets/css/meanmenu.css">
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="assets/css/boxicons.min.css">
        <!-- Flaticon CSS -->
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="assets/css/nice-select.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <!-- Owl Carousel Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		
		<title>MyHozDom.Ru - сайт о домашнем хозяйстве</title>

        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    </head>

    <body>
	
	<div class="navbar-area">
            <div class="main-responsive-nav">
                <div class="container">
                    <div class="main-responsive-menu">
                        <div class="logo">
                            <a href="index.php">
                                <img src="assets/img/logo.png" alt="image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="index.php">
                            <img src="assets/img/logo.png" alt="image">
                        </a>

                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        На основной сайт
                                    </a>
                                </li>

                            </ul>

                        </div>
                    </nav>
                </div>
            </div>

            <div class="others-option-for-responsive">
                <div class="container">
                    <div class="dot-menu">
                        <div class="inner">
                            <div class="circle circle-one"></div>
                            <div class="circle circle-two"></div>
                            <div class="circle circle-three"></div>
                        </div>
                    </div>
                    
                    <div class="container">
                        
                    </div>
                </div>
            </div>
        </div>






        <div class="main-banner">
            <div class="main-banner-item-lend">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="main-banner-content">
                                
                                <h1>Форум на тему сельского хозяйства</h1>
                                <p>Проект заключается в том, что необходимо создать крупную платформу на сельскохозяйственную тематику. По задумке разработчиков MySelhozDom может в себя включать:</p>
								<p>Форум для обсуждений</p>
								<p>Магазин</p>
								<p>Мессенжер либо мини-соц. сеть</p>
								<p>Площадку для размещения объявлений</p>
								<p>Видеоконференции</p>
								<p>Видеохостинг</p>
                                <div class="banner-btn">
                                    <a href="index.php" class="optional-btn">На основной сайт</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
		<section class="client-area ptb-100">
            <div class="container">
                <div class="section-title">
                    <h2>Участники проекта</h2>
                    <p>Со временем этот блок будет наполняться новыми участниками</p>
                </div>

                <div class="client-slider owl-carousel owl-theme">
                    <div class="client-item">
                        <div class="icon">
                        </div>

                        

                        <div class="client-info">
                            <img src="assets/img/lend/autorlend.jpg" alt="image">
                            <h3>Орлов Владислав</h3>
                            <span>Разработчик</span>
                        </div>
						<p>Роль в проекте:</p>
						<p>Верстка шаблона сайта</p>
						<p>Создание БД и таблиц</p>
						<p>Бэкенд разработка</p>
						<p>Разработка плана сайта</p>
						<p>Создание постера,поста и др.</p>
						<p>Создание, оформление отчетной документации</p>
                    </div>
                </div>
            </div>
        </section>
		
		
		
		 <section class="why-choose-area-lend pt-100">
            <div class="container">
                <div class="why-choose-item">
                    <div class="content">
                        
                        <h3>О резутьтатах на данный момент</h3>
                        
                    </div>

                    <div class="inner-content">
                        <div class="icon">
                            <i class="flaticon-leaf"></i>
                        </div>
                        <h4>Проект в начале своего пути</h4>
                        <p>Сайт проекта уже существует. В нем реализованы некоторые функции: создание, вывод обсуждений, авторизация, регистрация, востановление пароля, категории и др.</p>
                    </div>

                    <div class="inner-content">
                        <div class="icon">
                            <i class="flaticon-heart"></i>
                        </div>
                        <h4>Мы надеемся что наш проект будет дальше двигаться!</h4>
                        <p>Продолжаем разрабатывать проект. Ждем новых людей</p>
                    </div>

                    <div class="inner-content">
                        <div class="icon">
                            <i class="flaticon-plant"></i>
                        </div>
                        <h4>На данный момент</h4>
                        <p>Разработка проекта продолжается, в скором времени на сайте появятся новые функции, страницы, сервисы.</p>
                    </div>
                </div>
            </div>
        </section>




        
		       <!-- Start Go Top Area -->
        <div class="go-top">
            <i class='bx bx-up-arrow-alt'></i>
        </div>
        <!-- End Go Top Area -->
		
		
		<div class="copyright-area">
            <div class="container">
                <div class="copyright-area-content">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <p>
                                <i class="far fa-copyright"></i> 
                                Все права защищены @2021 SelhozDom.
                                
                            </p>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <ul>
                                <li>
                                    <a href="">GIT</a>
                                </li>
                                <li>
                                    <a href="index.php">Основной сайт</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       

        <!-- Jquery Slim JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Meanmenu JS -->
        <script src="assets/js/jquery.meanmenu.js"></script>
        <!-- Nice Select JS -->
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <!-- Owl Carousel JS -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Magnific Popup JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Odometer JS -->
        <script src="assets/js/odometer.min.js"></script>
        <!-- Jquery Appear JS -->
        <script src="assets/js/jquery.appear.min.js"></script>
        <!-- Ajaxchimp JS -->
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<!-- Form Validator JS -->
		<script src="assets/js/form-validator.min.js"></script>
		<!-- Contact JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Wow JS -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Custom JS -->
        <script src="assets/js/main.js"></script>
    </body>
</html>