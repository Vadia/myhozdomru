<?php ?>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Парсинг</title>
    <link rel="stylesheet" href="assets/np.css" media="screen">
<link rel="stylesheet" href="assets/Главная.css" media="screen">
    
	<script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script class="u-script" type="text/javascript" src="assets/np.js" defer=""></script>
	<script type="text/javascript" src="assets/parse.js"></script>
    
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    
    

    <meta property="og:title" content="Главная">
    <meta property="og:type" content="website">
    <meta name="theme-color" content="#478ac9">
  </head>
  <body class="u-body">
    <section class="u-clearfix u-section-1" id="sec-5a00">
      <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
        <div class="u-align-center u-form u-form-1">
          <form action="" class="u-clearfix u-form-spacing-10 u-form-vertical u-inner-form" style="padding: 10px" source="custom" name="form">
            <div class="u-form-group u-form-group-1">
              <input type="text" placeholder="Кол-во статей" id="input-text-1" name="text" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
            </div>
            <div class="u-align-center u-form-group u-form-submit">
              <a id="find-btn-1" class="u-btn u-btn-submit u-button-style">Найти</a>
            </div>
           <div id="errors-1"></div>
          </form>
        </div>
      </div>
    </section>
    <section class="u-align-center u-clearfix u-grey-5 u-section-2" id="sec-dfc8">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-list u-list-1">
          <div id="set-new-stat" class="u-repeater u-repeater-1">

            

           
          </div>
        </div>
      </div>
    </section>
    
    
    
  </body>
</html>