-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 20 2022 г., 19:02
-- Версия сервера: 5.5.68-MariaDB
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u913062_selhoz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` date DEFAULT NULL,
  `img` int(11) DEFAULT NULL,
  `src` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `imgsrc` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `vievs` int(20) DEFAULT NULL,
  `owner` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `category`, `date`, `img`, `src`, `imgsrc`, `vievs`, `owner`, `status`) VALUES
(37, 'Защищаем подсолнечник от сорняков, вредителей и болезней.', 'Растениеводство', '2021-06-26', NULL, '/articles/49m6549b36b366yam0656yf55f49.html', '/articles/img/54096547671284213013.jpg', 3, 'Hash', 1),
(41, 'Гербицидная защита картофеля', 'Растениеводство', '2021-06-27', NULL, '/articles/65u65n0a1265fbc165dq49al.html', '/articles/img/5888588095pic0.jpg', NULL, 'Hash', 1),
(42, 'Аграрии Республики Татарстан рассказали про особенности выращивания томатов', 'Растениеводство', '2021-06-27', NULL, '/articles/v99i1ro55494955l8736glaw6565a.html', '/articles/img/2184144718sd.jpeg', NULL, 'Hash', 1),
(43, 'Полезные свойства черешни', 'Органическая пища', '2021-06-27', NULL, '/articles/l9i4abf12pi49t49m5egkll.html', '/articles/img/9308975793231.jpg', 7, 'Hash', 1),
(44, 'В Арктической зоне Коми развивается фермерство', 'Животноводство', '2021-06-27', NULL, '/articles/bn4v9c87t87wnj6h7cj65e5e40.html', '/articles/img/3414505388hhh.jpg', NULL, 'anna.golik1107201', 1),
(46, 'Болезни озимых зерновых: разновидности, как выявить и как бороться', 'Растениеводство', '2021-06-27', NULL, '/articles/3s4lt126y0fnnv9c65q3612x5n.html', '/articles/img/4082943767пшеница.png', 2, 'anna.golik1107201', 1),
(47, 'Как тратить меньше времени на сбор урожая? Поставить на комбайн робота-автопилота!', 'Сельхоз Техника', '2021-06-27', NULL, '/articles/t43s49amcl4f14v949dv9p5e.html', '/articles/img/9532419562машиа.jpg', NULL, 'anna.golik1107201', 1),
(48, 'Хризантема – популярная срезка', 'Растениеводство', '2021-06-27', NULL, '/articles/55lninm12b4q12ika9iudm.html', '/articles/img/3681146674beppie_rosa_-_sort_privlekatelnoy_belo-krasnoy_okraski_vysota_rasteniy_okolo_80_sm_cvetet_v_oktyabre.jpg', NULL, 'anna.golik1107201', 1),
(49, 'Доильный аппарат - не роскошь, а необходимость', 'Животноводство', '2021-06-27', NULL, '/articles/5e9i5e36v91rlfnkf3s5eof44912.html', '/articles/img/159426492fermerruforbelagro3mini.jpg', NULL, 'anna.golik1107201', 1),
(50, 'ВЫРАЩИВАНИЕ И РАЗВЕДЕНИЕ ФОРЕЛИ В ПРУДАХ', 'Рыбоводство', '2021-06-27', NULL, '/articles/0q05e1rpx5v9f87i49cx5g12h749.html', '/articles/img/8783053880форель-озерная1.jpg', NULL, 'anna.golik1107201', 1),
(51, 'Почему в инкубаторе не вылупились цыплята?', 'Птицеводство', '2021-06-27', NULL, '/articles/3614f9i87d4b6y1rc1ri875e87z5.html', '/articles/img/7849277427H07cc581aa3934e83a0109c689b6133c4L.jpg', 1, 'anna.golik1107201', 1),
(52, 'Пятёрка лучших минитракторов в магазинах сельхозтехники в 2019-2020 гг.', 'Сельхоз Техника', '2021-06-27', NULL, '/articles/x5az555h73sa1r496536a1rip45ew.html', '/articles/img/96417073871_1.jpg', NULL, 'anna.golik1107201', 1),
(53, 'Обработка клубней картофеля от болезней и вредителей', 'Растениеводство', '2021-06-27', NULL, '/articles/49t9iid0a13sh712v91o1ra3s4.html', '/articles/img/346423068main.jpg', NULL, 'anna.golik1107201', 1),
(54, 'Черный чеснок', 'Растениеводство', '2021-06-27', NULL, '/articles/p4x5omath7lkb55j6l5e3sq5e.html', '/articles/img/2772474761chernyychesnok.jpg', NULL, 'anna.golik1107201', 1),
(55, 'Мыть или не мыть загрязненные инкубационные яйца?', 'Птицеводство', '2021-06-27', NULL, '/articles/436adau055u0l6yq1oh7fk.html', '/articles/img/29280215203730.jpg', NULL, 'anna.golik1107201', 1),
(65, 'Проверочная статья', 'Растениеводство', '2022-01-05', NULL, '/articles/ict5e364mucuim6y65114g.html', '/articles/img/9410575064Многогранник.png', 3, 'Admin', 0),
(66, 'Стимул', 'Сельхоз Техника', '2022-01-19', NULL, '/articles/ph75e36m49871rwz54mnh70k1r6y.html', '/articles/img/6833676141Screenshot_1.png', 3, 'Admin', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) unsigned NOT NULL,
  `autor` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `src` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `autor`, `date`, `text`, `src`, `login`) VALUES
(71, 'Владислав Орлов', '02.07.2021 В 16:48', 'Спасибо за статью!', '/articles/49m6549b36b366yam0656yf55f49.html', 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `counter`
--

CREATE TABLE IF NOT EXISTS `counter` (
  `id` int(11) NOT NULL,
  `klientip` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `data` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `blog` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `counter`
--

INSERT INTO `counter` (`id`, `klientip`, `data`, `blog`) VALUES
(16, '92.38.12.159', '20220105', '37'),
(17, '92.38.12.159', '20220105', '65'),
(18, '87.250.224.190', '20220105', '65'),
(19, '95.108.213.38', '20220105', '65'),
(20, '92.38.12.159', '20220106', '46'),
(21, '95.153.169.141', '20220106', '43'),
(22, '95.153.169.141', '20220106', '37'),
(23, '87.250.224.190', '20220106', '46'),
(24, '92.38.12.159', '20220106', '37'),
(25, '46.161.11.177', '20220115', '43'),
(26, '46.161.11.177', '20220116', '43'),
(27, '46.161.11.177', '20220117', '43'),
(28, '46.161.11.177', '20220118', '43'),
(29, '87.250.224.46', '20220119', '43'),
(30, '92.38.12.159', '20220119', '66'),
(31, '5.255.253.111', '20220119', '66'),
(32, '92.38.12.159', '20220120', '66'),
(33, '92.38.12.159', '20220120', '43'),
(34, '92.38.12.159', '20220120', '51');

-- --------------------------------------------------------

--
-- Структура таблицы `unfinishedstat`
--

CREATE TABLE IF NOT EXISTS `unfinishedstat` (
  `id` int(11) NOT NULL,
  `autor` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `src` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `unfinishedstat`
--

INSERT INTO `unfinishedstat` (`id`, `autor`, `name`, `date`, `category`, `src`) VALUES
(5, 'Admin', 'Проверочная статья #1', '2022-01-20', 'Животноводство', '/articles/unfinisedstats/ib6y49mh76yj636z5z5g41rc4l1rAdmin.html');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `famil` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lvl` int(5) DEFAULT NULL,
  `changepass` int(11) DEFAULT NULL,
  `aktiv` int(20) DEFAULT NULL,
  `reenter` int(11) DEFAULT NULL,
  `rekey` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `changepassdate` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploadimg` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateupload` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastlogin` int(12) DEFAULT NULL,
  `groupuser` int(5) DEFAULT NULL,
  `phonenumber` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `name`, `famil`, `password`, `lvl`, `changepass`, `aktiv`, `reenter`, `rekey`, `changepassdate`, `uploadimg`, `dateupload`, `lastlogin`, `groupuser`, `phonenumber`, `city`) VALUES
(5, 'Admin', 'vadd.orov@gmail.com', 'Владислав', 'Орлов', '$2y$10$MREonDaW1kddrUifzhs4C.e6wylG6u2bTksAn1luEcz9O.ZV02psC', NULL, 0, NULL, 0, '', '20210701', NULL, NULL, 20220120, 3, '+7999999999', 'Москва'),
(6, 'Hash', 'vadia.orlov2015@yandex.ru', 'Владимир', 'Котов', '$2y$10$uULXLasl591Cmeex.cHXDOPu7gyqPPuVFiiJWzbtwnYZ2.FWN5g16', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 20220120, 1, NULL, NULL),
(7, 'anna.golik1107201', 'anna.golik11072001@mail.ru', 'Ирина', 'Воробьева', '$2y$10$Jv5Get5YEvfVrl/CouBWIehUjDLV9bbaVOKwWExp2O7fjKJvKzQVi', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 20210627, 1, NULL, NULL),
(8, 'Moder', 'vaddd.orov@gmail.com', 'Андрей', 'Козлов', '$2y$10$N/MPydYGttcjC2ut9y1ktObYguZPN/R872p4YIktTu9vH7hD2skwy', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 20220120, 2, NULL, NULL),
(9, 'Douglasjah', 'inscoryb80@yandex.ru', 'Douglasjah', '232', '$2y$10$AcqEh3Tecs/VjMAhADyU4ukpWROQshRAFFYC2e1/T0PEadx44tLCm', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 20211029, 1, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `unfinishedstat`
--
ALTER TABLE `unfinishedstat`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `counter`
--
ALTER TABLE `counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `unfinishedstat`
--
ALTER TABLE `unfinishedstat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
