<?php
require '../db/db.php';


 ?>
<html lang="ru">
  <head>
    <title>ДОСКА ОБЬЯВЛЕНИЙ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="assets/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="assets/css/aos.css">
    <link rel="stylesheet" href="assets/css/rangeslider.css">

    <link rel="stylesheet" href="assets/css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar container py-0 bg-white" role="banner">

      
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.html" class="text-black mb-0">Доска<span class="text-primary">Объявлений</span>  </a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
		  
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
			  
                <li class="active"><a href="../index.php">На главную</a></li>
                

			   
			   <?php if(isset($_SESSION['logged_user'])): ?>
			   <li class="has-children"><a href=""><?php echo $_SESSION['logged_user']->name; ?></a>
                  <ul class="dropdown">
                    <li><a href="../logout.php">Выйти</a></li>
                  </ul> 
			   </li>
			   <?php else: ?>
                <li class="ml-xl-3 login"><a href="../login.php"><span class="border-left pl-xl-4"></span>Войти</a></li>
                <li><a href="../register.php">Регистрация</a></li>
				<?php endif; ?>

                <li><a href="#" class="cta"><span class="bg-primary text-white rounded">Создать объявление</span></a></li>
              </ul>
            </nav>
			
          </div>


          <div class="d-inline-block d-xl-none ml-auto py-3 col-6 text-right" style="position: relative; top: 3px;">
            <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
          </div>

        </div>
      
      
    </header>

  

    <div class="site-blocks-cover overlay" style="background-image: url(assets/images/hero_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-12">
            
            
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1 class="" data-aos="fade-up">Доска объявлений</h1>
                <p data-aos="fade-up" data-aos-delay="100">Разместите ваше объявление. Его увидят множество пользователей</p>
              </div>
            </div>

            <div class="form-search-wrap" data-aos="fade-up" data-aos-delay="200">
              <form method="post">
                <div class="row align-items-center">
                  <div class="col-lg-12 mb-4 mb-xl-0 col-xl-4">
                    <input type="text" class="form-control rounded" placeholder="Что вы ищете?">
                  </div>
                  <div class="col-lg-12 mb-4 mb-xl-0 col-xl-3">
                    <div class="wrap-icon">
                      <span class="icon icon-room"></span>
                      <input type="text" class="form-control rounded" placeholder="Москва">
                    </div>
                    
                  </div>
                  <div class="col-lg-12 mb-4 mb-xl-0 col-xl-3">
                    <div class="select-wrap">
                      <span class="icon"><span class="icon-keyboard_arrow_down"></span></span>
                      <select class="form-control rounded" name="" id="">
                        <option value="">Все категории</option>
                        <option value="">Недвижимость</option>
                        <option value="">Сельхоз продукция</option>
                        <option value="">Сельхоз техника</option>
                        <option value="">Готовая продукция</option>
                        <option value="">Оборудование</option>
                        <option value="">Животные</option>
						<option value="">Растения</option>
						<option value="">Удобрения</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-12 col-xl-2 ml-auto text-right">
                    <input type="submit" class="btn btn-primary btn-block rounded" value="Найти">
                  </div>
                  
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>  

    <div class="site-section bg-light">
      <div class="container">
        
        <div class="overlap-category mb-5">
          <div class="row align-items-stretch no-gutters">
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Недвижимость</span>
                <span class="number">3,921</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Продукты</span>
                <span class="number">398</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Техника</span>
                <span class="number">1,229</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Животные</span>
                <span class="number">32,891</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Растения</span>
                <span class="number">29,221</span>
              </a>
            </div>
            <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
              <a href="#" class="popular-category h-100">
                <span class="icon"><span class="flaticon-house"></span></span>
                <span class="caption mb-2 d-block">Удобрения</span>
                <span class="number">219</span>
              </a>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-12">
            <h2 class="h5 mb-4 text-black">Новые объявления</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-12  block-13">
            <div class="owl-carousel nonloop-block-13">
              
              <div class="d-block d-md-flex listing vertical">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img1.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Животные</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Корова</a></h3>
                  <address>Москва - 10 км от вас</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>

              <div class="d-block d-md-flex listing vertical">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img2.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Растения</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Подсолнечник</a></h3>
                  <address>Москва - 25,2 км от вас</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>

              <div class="d-block d-md-flex listing vertical">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img3.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Техника</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Трактор</a></h3>
                  <address>Москва - 12 км от вас</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>

              <div class="d-block d-md-flex listing vertical">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img4.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Продукты</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Яйца</a></h3>
                  <address>Москва - 3 км от вас</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>

              <div class="d-block d-md-flex listing vertical">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img5.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Удобрения</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Азотное удобрение</a></h3>
                  <address>Москва - 31 км от вас</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>


            </div>
          </div>


        </div>
      </div>
    </div>
    
    <div class="site-section" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
            <h2 class="font-weight-light text-primary">Популярные объявления</h2>
            <p class="color-black-opacity-5">Топ 2 самых популярных обьявлений</p>
          </div>
        </div>

        <div class="row">
          


          <div class="col-md-6 mb-4 mb-lg-4 col-lg-6">
            
            <div class="listing-item">
              <div class="listing-image">
                <img src="assets/images/img4.jpg" alt="Image" class="img-fluid">
              </div>
              <div class="listing-item-content">
                <a href="#" class="bookmark" data-toggle="tooltip" data-placement="left" title="Bookmark"><span class="icon-heart"></span></a>
                <a class="px-3 mb-3 category" href="#">Продукты</a>
                <h2 class="mb-1"><a href="#">Яйца</a></h2>
                <span class="address">Москва - 3 км от вас</span>
              </div>
            </div>

          </div>
          <div class="col-md-6 mb-4 mb-lg-4 col-lg-6">
            
            <div class="listing-item">
              <div class="listing-image">
                <img src="assets/images/img5.jpg" alt="Image" class="img-fluid">
              </div>
              <div class="listing-item-content">
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <a class="px-3 mb-3 category" href="#">Удобрения</a>
                <h2 class="mb-1"><a href="#">Азотное удобрение</a></h2>
                <span class="address">Москва - 31 км от вас</span>
              </div>
            </div>

          </div>
          

        </div>
      </div>
    </div>


    <div class="site-section bg-light">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-7 text-left border-primary">
            <h2 class="font-weight-light text-primary">Новые объявления</h2>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-lg-6">

            <div class="d-block d-md-flex listing">
              <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img2.jpg')"></a>
              <div class="lh-content">
                <span class="category">Растения</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="listings-single.html">Подсолнечник</a></h3>
                <address>Москва</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(Рейтинг)</span>
                </p>
              </div>
            </div>
            <div class="d-block d-md-flex listing">
                <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img3.jpg')"></a>
                <div class="lh-content">
                  <span class="category">Техника</span>
                  <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                  <h3><a href="listings-single.html">Трактор</a></h3>
                  <address>Москва</address>
                  <p class="mb-0">
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-warning"></span>
                    <span class="icon-star text-secondary"></span>
                    <span class="review">(Рейтинг)</span>
                  </p>
                </div>
              </div>

              

             

          </div>
          <div class="col-lg-6">

            <div class="d-block d-md-flex listing">
              <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img1.jpg')"></a>
              <div class="lh-content">
                <span class="category">Животные</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="listings-single.html">Корова</a></h3>
                <address>Москва</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(Рейтинг)</span>
                </p>
              </div>
            </div>

            <div class="d-block d-md-flex listing">
              <a href="listings-single.html" class="img d-block" style="background-image: url('assets/images/img5.jpg')"></a>
              <div class="lh-content">
                <span class="category">Удобрения</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="listings-single.html">Азотное удобрение</a></h3>
                <address>Москва</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(Рейтинг)</span>
                </p>
              </div>
            </div>
            

          </div>
        </div>
      </div>
    </div>
    
    <footer class="site-footer">
      <div class="container">
	  
	  
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
            <p>
            
            Copyright &copy;2021 Все права защищены | by <a href="" target="_blank" >ORLOV</a>
            
            </p>
            </div>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="assets/js/jquery-ui.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/jquery.stellar.min.js"></script>
  <script src="assets/js/jquery.countdown.min.js"></script>
  <script src="assets/js/jquery.magnific-popup.min.js"></script>
  <script src="assets/js/bootstrap-datepicker.min.js"></script>
  <script src="assets/js/aos.js"></script>
  <script src="assets/js/rangeslider.min.js"></script>

  <script src="assets/js/main.js"></script>
    
  </body>
</html>