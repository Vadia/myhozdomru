<?php
require 'db/db.php';
$data = $_POST;
$mess = 0;
$src = $_GET['src'];
$unfinbd = R::findOne('unfinishedstat', 'autor = ?', array($_SESSION['logged_user']->login));
?>
<html>
<head>
	<!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Animate CSS --> 
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="assets/css/meanmenu.css">
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="assets/css/boxicons.min.css">
        <!-- Flaticon CSS -->
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="assets/css/nice-select.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <!-- Owl Carousel Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		
		<title>MyHozDom.Ru - сайт о домашнем хозяйстве</title>

        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
</head>
<body>






<?php if ( isset ($_SESSION['logged_user']) ) : ?>

<div id="intro">

 <!-- Start Preloader Area -->
        <div class="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- End Preloader Area -->

        <!-- Start Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <ul class="top-header-information">
                            <li>
                                <a href="index.php">Назад</a>
                            </li>
							
                            <li>
                                <div class="form-group">
								<?php if(isset($unfinbd)): ?>
								  <input type="text" id="nameofstat" class="form-control" name="name" value="<?php echo $unfinbd->name; ?>">
								<?php else : ?>
                                  <input type="text" id="nameofstat" class="form-control" name="name" placeholder="Название статьи">
								<?php endif; ?>
                                </div>
                            </li>
							<li>
                                <button type="submit" id="dopublicstat" class="btn">Опубликовать</button>
                            </li>
                        </ul>
                    </div>
                  
                    <div class="col-lg-6">
                        <ul class="top-header-social">
						<?php if ( isset ($_SESSION['logged_user']) ) : ?>

						    <li>
                               <h6><?php echo $_SESSION['logged_user']->name ?>  <?php echo substr($_SESSION['logged_user']->famil,0,2) ?>.</h6>
                            </li>
							
							<li>
                               <a href="logout.php">Выйти</a>
                            </li>
						
						<?php else : header('Location: ../login.php '); ?>
						
                           
							<?php endif; ?>
                        </ul>
                    </div>
			      	
                </div>
            </div>
        </div>

</div>
<br />



<div id="sample">
<script type="text/javascript" src="assets/redactor/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<center><div id="errorslist" style="color:red;"></div></center><hr>

						

       <input id="mainphotostat" class="btn" type="file">


<select id="selectedcateg" name="category">
    <option selected value="none">Выберете категорию</option>
    <option value="Растениеводство">Растениеводство</option>
	
	
    <option value="Животноводство">Животноводство</option>
	
	
    <option value="Птицеводство">Птицеводство</option>
	
	
	<option value="Сельхоз Техника">Сельхоз Техника</option>
	
	
	<option value="Строительство">Строительство</option>
	
	
	<option value="Рыбоводство">Рыбоводство</option>
	
	
	<option value="Органическая пища">Органическая пища</option>
	
	
   </select><br><br><br>

<?php if(isset($src)): 



?>
<textarea id="textarea" style="width: 100%;">
<?php include_once ('parse/php/download.php'); ?>
</textarea>
<?php else : ?>

<?php if(isset($unfinbd)): ?>
<textarea id="textarea" style="width: 100%;">
<?php echo file_get_contents(__DIR__ . $unfinbd->src); ?>
</textarea>
 
<?php else : ?>
<textarea id="textarea" style="width: 100%;">
ТЕКСТ
</textarea>
<?php endif; ?>
<?php endif; ?>

</div>
<?php else : header('Location: /login.php'); ?>
<?php endif; ?>
 <!-- End QuickView Modal Area -->

        <!-- Jquery Slim JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Meanmenu JS -->
        <script src="assets/js/jquery.meanmenu.js"></script>
        <!-- Nice Select JS -->
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <!-- Owl Carousel JS -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Magnific Popup JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Odometer JS -->
        <script src="assets/js/odometer.min.js"></script>
        <!-- Jquery Appear JS -->
        <script src="assets/js/jquery.appear.min.js"></script>
        <!-- Ajaxchimp JS -->
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<!-- Form Validator JS -->
		<script src="assets/js/form-validator.min.js"></script>
		<!-- Contact JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Wow JS -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Custom JS -->
        <script src="assets/js/main.js"></script>
		
		<script src="assets/js/writescript.js"></script>
</body>
</html>