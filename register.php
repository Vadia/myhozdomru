<?php 
require 'db/db.php';

    $mess = 0;
	$data = $_POST;



	//если кликнули на button
	if ( isset($data['do_signup']) )
	{
    // проверка формы на пустоту полей
		$errors = array();
		if ( trim($data['login']) == '' )
		{
			$errors[] = 'Введите логин';
		}

		if ( trim($data['email']) == '' )
		{
			$errors[] = 'Введите Email';
		}
		
		if ( trim($data['name']) == '' )
		{
			$errors[] = 'Введите Имя';
		}
		
		if ( trim($data['famil']) == '' )
		{
			$errors[] = 'Введите Фамилию';
		}

		if ( $data['password'] == '' )
		{
			$errors[] = 'Введите пароль';
		}

		if ( $data['password_2'] != $data['password'] )
		{
			$errors[] = 'Повторный пароль введен не верно!';
		}

		//проверка на существование одинакового логина
		if ( R::count('users', "login = ?", array($data['login'])) > 0)
		{
			$errors[] = 'Пользователь с таким логином уже существует!';
		}
    
    //проверка на существование одинакового email
		if ( R::count('users', "email = ?", array($data['email'])) > 0)
		{
			$errors[] = 'Пользователь с таким Email уже существует!';
		}






		if ( empty($errors) )
		{
			//ошибок нет, теперь регистрируем
			
			
			
			
			$user = R::dispense('users');
			$user->login = $data['login'];
			$user->email = $data['email'];
			$user->name = $data['name'];
			$user->famil = $data['famil'];
			$user->password = password_hash($data['password'], PASSWORD_DEFAULT); //пароль нельзя хранить в открытом виде, мы его шифруем при помощи функции password_hash для php > 5.6
			$user->groupuser = "1";
            R::store($user);
			
			
			echo '<div style="color:dreen;">Вы успешно зарегистрированы!</div><hr>';
			header('Location: login.php ');
		}else
		{
			$mess = 1;
			//вывод ошибок
			//echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
		}

	}





?>
<!doctype html>
<html lang="zxx">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Animate CSS --> 
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="assets/css/meanmenu.css">
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="assets/css/boxicons.min.css">
        <!-- Flaticon CSS -->
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="assets/css/nice-select.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <!-- Owl Carousel Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		
		<title>---</title>

        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    </head>

    <body>
        
        <!-- Start Preloader Area -->
        <div class="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
            </div>
        </div>
       

         



		 
       


        <section class="login-area ptb-100">
            <div class="container">
                <div class="login-form">
				<a href="index.php"><h5>Назад</h5></a><br>
                    <h2>Регистрация</h2>
					
					<?php
						 if ($mess == 1) {
						echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
						 }
						?>

                    <form action="/register.php" method="POST" >
                        <div class="form-group">
                            
                            <input type="text" class="form-control" name="login" placeholder="Введите логин" value="<?php echo @$data['login']; ?>">
                        </div>
						
						 <div class="form-group">
                            
                            <input type="text" class="form-control" name="email" placeholder="Введите ваш email" value="<?php echo @$data['email']; ?>">
                        </div>
						
						<div class="form-group">
                            
                            <input type="text" class="form-control" name="name" placeholder="Введите ваше имя" value="<?php echo @$data['name']; ?>">
                        </div>
						
						<div class="form-group">
                            
                            <input type="text" class="form-control" name="famil" placeholder="Введите вашу фамилию" value="<?php echo @$data['famil']; ?>">
                        </div>

                        <div class="form-group">
                            
                            <input type="password" class="form-control" name="password" placeholder="Введите пароль" value="<?php echo @$data['password']; ?>">
                        </div>
						
						 <div class="form-group">
                            
                            <input type="password" class="form-control" name="password_2" placeholder="Повторите пароль" value="<?php echo @$data['password_2']; ?>">
                        </div>


                        

                        <button type="submit" class="default-btn" name="do_signup">Зарегистрироваться</button>
                    </form>
                </div>
            </div>
        </section>








        <!-- Start Go Top Area -->
        <div class="go-top">
            <i class='bx bx-up-arrow-alt'></i>
        </div>
        <!-- End Go Top Area -->
        
        <!-- Jquery Slim JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Meanmenu JS -->
        <script src="assets/js/jquery.meanmenu.js"></script>
        <!-- Nice Select JS -->
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <!-- Owl Carousel JS -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Magnific Popup JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Odometer JS -->
        <script src="assets/js/odometer.min.js"></script>
        <!-- Jquery Appear JS -->
        <script src="assets/js/jquery.appear.min.js"></script>
        <!-- Ajaxchimp JS -->
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<!-- Form Validator JS -->
		<script src="assets/js/form-validator.min.js"></script>
		<!-- Contact JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Wow JS -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Custom JS -->
        <script src="assets/js/main.js"></script>
    </body>
</html>