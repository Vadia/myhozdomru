package com.example.myapplication;

import androidx.annotation.ContentView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Barrier;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;


public class MainActivity extends AppCompatActivity {

    TextView myTextView;
    WebView HtmlTextWeb;
    Button myButton;
    Button myButton1;
    Button myButton2;
    PostGoLogin mt;
    GetBlogs GB;
    GetBlogsByCateg GBBC;
    NewRegister NR;
    String Choice;




    public void checkLogPass(){

    setContentView(R.layout.activity_main);
    myTextView = (TextView) findViewById(R.id.textView);
    myButton = (Button) findViewById(R.id.button);

    myButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            EditText ETlogin = (EditText)findViewById(R.id.textLogin);
            EditText ETpass = (EditText)findViewById(R.id.textPassword);

            String login = ETlogin.getText().toString();
            String pass = ETpass.getText().toString();

            myTextView.setText("Проверочка..");
            String response = "";

            mt = new PostGoLogin();
            mt.execute(response,login,pass);

            String sss = "";
            try {
                sss = mt.get().toString();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(sss.equals("JagaJagaOk")){
                setContentView(R.layout.after_login);
                GiveBlogs();
            }else{
                myTextView.setText("Неверные данные");
            }


        }
    });

        myButton = (Button) findViewById(R.id.button4);

        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.after_login);
                GiveBlogs();
            }
        });

        myButton = (Button) findViewById(R.id.button5);

        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.register);
                DoRegister();
            }
        });


}

//регистрация
    public void DoRegister(){

        myButton = (Button) findViewById(R.id.buttonReg);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText RegLogin = (EditText) findViewById(R.id.RegLogin);
                EditText RegEmail = (EditText) findViewById(R.id.RegEmail);
                EditText RegPass = (EditText) findViewById(R.id.RegPass);

                String Rlogin = RegLogin.getText().toString();
                String Remail = RegEmail.getText().toString();
                String Rpass = RegPass.getText().toString();

                NR = new NewRegister();
                NR.execute(Rlogin,Remail,Rpass);

                setContentView(R.layout.activity_main);

            }
        });

        myButton = (Button) findViewById(R.id.button8);
        myButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            checkLogPass();
                                        }
        });


    }


//диалог фильтр
    public void CreateDialogFilter() {
        final String[] catNamesArray = {"Растениеводство", "Животноводство", "Птицеводство", "Сельхоз Техника", "Строительство", "Рыбоводство", "Органическая Пища"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Выберите категорию")
                // добавляем переключатели
                .setSingleChoiceItems(catNamesArray, -1,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int item) {
                                Toast.makeText(
                                        MainActivity.this,
                                        "Выбранная категория: "
                                                + catNamesArray[item],
                                        Toast.LENGTH_SHORT).show();
                                Choice = catNamesArray[item];

                            }
                        })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                      GiveOutCategory();

                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

         builder.show();
    }


    public void GiveOutCategory(){

        GBBC = new GetBlogsByCateg();
        GBBC.execute(Choice, "", "");

        String ReturningBlogs = "";
        try {
            ReturningBlogs = GBBC.get().toString();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.after_login);
        DisplayBlogs(ReturningBlogs);
    }

    public void GiveBlogs(){
        String response = "";
        GB = new GetBlogs();
        GB.execute(response, "", "");

        String ReturningBlogs = "";
        try {
            ReturningBlogs = GB.get().toString();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DisplayBlogs(ReturningBlogs);

    }


    public void DisplayBlogs(String fromrequest){

        String[] blogs = fromrequest.split("###");


        final LinearLayout ll = (LinearLayout) findViewById(R.id.Linear1);


        for(int i=0; i < blogs.length; i++) {

            String[] imgtextandsrc = blogs[i].toString().split("&&&");

            ImageButton btn = new ImageButton(this);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setContentView(R.layout.read_blog);
                    String SrcHtmlDoc = "https://selhozdom.ru" + imgtextandsrc[1];


                    HtmlTextWeb = (WebView) findViewById(R.id.webView);
                    HtmlTextWeb.getSettings().setJavaScriptEnabled(true);
                    HtmlTextWeb.setWebViewClient(new WebViewClient());


                    HtmlTextWeb.loadUrl(SrcHtmlDoc);

                    myButton1 = (Button) findViewById(R.id.button3);
                    myButton1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setContentView(R.layout.after_login);
                            if(Choice != null){
                                GiveOutCategory();
                            }else {
                                GiveBlogs();
                            }
                        }
                    });


                }
            });

            String[] imgandtext = imgtextandsrc[0].toString().split("&&");

            // текст imgandtext[1]
            TextView textblog = new TextView(this);
            textblog.setText(imgandtext[1]);
            textblog.setTextSize(14);
            textblog.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textblog.setAllCaps(true);


            btn.setBackgroundColor(0xffffffff);
            Picasso.get().load("https://selhozdom.ru" + imgandtext[0]).resize(500, 300).into(btn);

            btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ll.addView(btn);
            ll.addView(textblog);
        }

        myButton2 = (Button) findViewById(R.id.button2);

        myButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogPass();
            }
        });

        myButton2 = (Button) findViewById(R.id.button6);

        myButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateDialogFilter();
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         checkLogPass();

    }
}

//Вход в приложение
class PostGoLogin extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL("https://selhozdom.ru/api/api.php?req=login");
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new SecureRandom());
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setReadTimeout(7000);
            conn.setConnectTimeout(7000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.connect();

            conn.getOutputStream().write(("name=" + params[1] +"&"+ "pass=" + params[2]).getBytes());




            //Ответ от сервера
            String result = new String();
            InputStream is = conn.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
            params[0] = result;

            conn.disconnect();



        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return params[0];
    }
}
//выдача всех блогов
class GetBlogs extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL("https://selhozdom.ru/api/api.php?req=getblogs");
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new SecureRandom());
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setReadTimeout(7000);
            conn.setConnectTimeout(7000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.connect();




            //Ответ от сервера
            String result = new String();
            InputStream is = conn.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
            params[0] = result;

            conn.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return params[0];
    }
}
//выдача блогов по категории
class GetBlogsByCateg extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL("https://selhozdom.ru/api/api.php?req=getblogsbycateg&categ="+params[0]);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new SecureRandom());
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setReadTimeout(7000);
            conn.setConnectTimeout(7000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.connect();




            //Ответ от сервера
            String result = new String();
            InputStream is = conn.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
            params[0] = result;

            conn.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return params[0];
    }
}
//регистрация пользователя
class NewRegister extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... params) {

        URL url = null;
        try {
            url = new URL("https://selhozdom.ru/api/api.php?req=register&login="+params[0]+"&email="+params[1]+"&pass="+params[2]);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new SecureRandom());
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setReadTimeout(7000);
            conn.setConnectTimeout(7000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.connect();




            //Ответ от сервера
            String result = new String();
            InputStream is = conn.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
            params[0] = result;

            conn.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return params[0];
    }
}


