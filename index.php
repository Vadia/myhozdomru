<?php 
require 'db/db.php';

$data = $_POST;

$schet = 0;
$allblogs = @$_GET['allblogs'];
$lk = @$_GET['lk'];
$blog = @$_GET['blog'];
$srcblog = @$_GET['src'];
$selectetcateg = @$_GET['category'];
$moderatestat = @$_GET['moderatestat'];


if(isset($srcblog)){
$autor = R::findOne('blogs', 'src = ?', array($srcblog));
$autoruser = R::findOne('users', 'login = ?', array($autor->owner));
}

$topblogs = R::getAll("SELECT * FROM `blogs` WHERE status = '1' ORDER BY `vievs` DESC ;");


?>
<!doctype html>
<html lang="ru">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Animate CSS --> 
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="assets/css/meanmenu.css">
        <!-- Boxicons CSS -->
        <link rel="stylesheet" href="assets/css/boxicons.min.css">
        <!-- Flaticon CSS -->
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="assets/css/nice-select.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <!-- Owl Carousel Default CSS -->
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		
		<?php if(isset($srcblog)): ?>
		<title><?php echo $autor->name; ?></title>
		<meta name="description" content="<?php echo $autor->name, " ", $autor->category; ?>">
		<meta name="keywords" content="<?php echo $autor->category; ?>"/>
		<?php else: ?>
		
		<title>SelHozDom.Ru - Фермерский сайт - форум сельское хозяйство - фермерское хозяйство сайт</title>
		<meta name="description" content="Сайт посвящен тематике сельского хозяйства. Вы сможете поделится интересной информацией на нашем сайте и обсудить ее с другими пользователями">
		<meta name="keywords" content="Фермерский сайт, сельское хозяйство, сельхоз форум, Птицеводство, Животноводство, Растениеводство, Рыбоводство"/>
		<?php endif; ?>
		
        <meta name="robots" content="index,follow" />
		
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
		<style>
		#hideid{
		font-size:0;
		}
		</style>
		
		
		<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(85152256, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/85152256" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
		
    </head>

    <body>

        <!-- Start Preloader Area -->
        <div class="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- End Preloader Area -->

        <!-- Start Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <ul class="top-header-information">
                            <li>
                                <i class='bx bxs-map'></i>
                                Москва
                            </li>
                            <li>
                                
                            </li>
                        </ul>
                    </div>
                  
                    <div class="col-lg-6">
                        <ul class="top-header-social">
						<?php if ( isset ($_SESSION['logged_user']) ) : ?>
						
						    <li>
                               <a href="write.php">Создать статью</a>
                            </li>
						
						    <li>
                               <h6 id="nameuser"><?php echo $_SESSION['logged_user']->name ?>  <?php echo substr($_SESSION['logged_user']->famil,0,2) ?>.</h6>
                            </li>
							
							<li>
                               <a href="logout.php">Выйти</a>
                            </li>
						
						<?php else : ?>
						
                            <li>
                                <a href="login.php">Войти</a>
                            </li>
                            <li>
                                <a href="register.php">Зарегистрироваться</a>
                            </li>
							<?php endif; ?>
                        </ul>
                    </div>
			      	
                </div>
            </div>
        </div>
        <!-- End Top Header Area -->

        <!-- Start Navbar Area -->
        <div class="navbar-area">
            <div class="main-responsive-nav">
                <div class="container">
                    <div class="main-responsive-menu">
                        <div class="logo">
                            <a href="index.php">
                                <img src="assets/img/logo.png" alt="image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="index.php">
                            <img src="assets/img/logo.png" alt="image">
                        </a>

                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link">
                                        Домой
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Обсуждения
                                        <i class='bx bx-chevron-down'></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="?allblogs=1" class="nav-link">
                                                Все Обсуждения
                                            </a>
                                        </li>
                                    </ul>
                                </li>
								<?php if ( isset ($_SESSION['logged_user']) ) : ?>
								<?php if($_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2"): ?>
								<li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Модераторская
                                        <i class='bx bx-chevron-down'></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="?allblogs=1&moderatestat=1" class="nav-link">
                                                Статьи на модерации
                                            </a>
                                        </li>
										<li class="nav-item">
                                            <a href="?allblogs=1&moderatestat=2" class="nav-link">
                                                Все Все статьи
                                            </a>
                                        </li>
                                    </ul>
                                </li>
								<?php endif; ?>
								<?php endif; ?>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Категории 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Растениеводство" class="nav-link">
                                                Растениеводство
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Животноводство" class="nav-link">
                                                Животноводство
                                            </a>
                                        </li>
										
										<li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Птицеводство" class="nav-link">
                                                Птицеводство
                                            </a>
                                        </li>


                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Сельхоз Техника" class="nav-link">
                                                Сельхоз Техника
                                            </a>
                                        </li>
										
										<li class="nav-item">
                                            <a href="" class="nav-link">
                                                Объявления
                                            </a>
                                        </li>


                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Строительство" class="nav-link">
                                                Строительство
                                            </a>
                                        </li>


                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Рыбоводство" class="nav-link">
                                                Рыбоводство
                                            </a>
                                        </li>


                                        <li class="nav-item">
                                            <a href="index.php?allblogs=1&category=Органическая Пища" class="nav-link">
                                                Органическая Пища
                                            </a>
                                        </li>


                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a href="board/board.php" class="nav-link">
                                        Объявления 
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Магазин 
                                    </a>

                                </li>
								<?php if ( isset ($_SESSION['logged_user']) ) : ?>

                                <li class="nav-item">
                                    <a href="index.php?lk=1" class="nav-link">
                                        Личный кабинет
                                    </a>
                                </li>
								<?php else : ?>
								<?php endif; ?>
								
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>

            <div class="others-option-for-responsive">
                <div class="container">
                    <div class="dot-menu">
                        <div class="inner">
                            <div class="circle circle-one"></div>
                            <div class="circle circle-two"></div>
                            <div class="circle circle-three"></div>
                        </div>
                    </div>
                    
                    <div class="container">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Navbar Area -->

       
	   
	   <?php if (isset($blog)) :?>
	   <?php include 'assets/php/counterviev.php'; ?>
	   <section class="blog-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="blog-details-desc">
                            <div class="article-image">
							<?php if($autor->category == "Птицеводство"): ?>
                                <img src="assets/img/ptiz.jpg" alt="image">
								<?php else : ?>
								<?php endif; ?>
                            </div>
							<br>
							<h1><?php echo $autor->name; ?></h1>

                            <div class="article-content">
                                <div class="entry-meta">
                                    <ul>
                                        <li>
                                            <span>Создано:</span> 
                                            <a href=""><?php echo $autor->date; ?></a>
                                        </li>
                                        <li>
                                            <span>Автор:</span> 
                                            <a href=""><?php echo $autoruser->name; ?>  <?php echo $autoruser->famil; ?></a>
                                        </li>
                                    </ul>
                                </div>
								
								
								
								
								 <p><?php echo file_get_contents(__DIR__ . $srcblog); ?></p>

                                

                                
                            </div>

                            <div class="article-footer">
                                <div class="article-tags">
                                    <span>
                                        <i class='bx bxs-bookmark'></i>
                                    </span>
                                    <a href="#">Тег-1</a>,
                                    <a href="#">Тег-2</a>
                                </div>

                                <div class="article-share">
                                    <ul class="social">
                                        <li><span>Поделиться:</span></li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-facebook'></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-twitter'></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-instagram'></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="post-navigation">
                                <div class="navigation-links">
                                    <div class="nav-previous">
                                        <a href="">
                                            <i class="flaticon-left-arrow"></i>
                                            Предыдущее обсуждение
                                        </a>
                                    </div>
                                    <div class="nav-next">
                                        <a href="">
                                            Следующее обсуждение 
                                            <i class="flaticon-right-arrow"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="comments-area">
                                <h3 class="comments-title">Комментарии:</h3>
                                <ol class="comment-list">
                                    <li class="comment">
									
<?php 
$startcomments = R::getAll("SELECT * FROM `comments` WHERE src='$srcblog';");

foreach ($startcomments as $ncomment):
?>
									
                                        <article id="S<?php echo $ncomment['id'];?>" class="comment-body">
                                            <footer class="comment-meta">
                                                <div class="comment-author vcard">
                                                    <img src="assets/img/client/kozochka.jpg" class="avatar" alt="image">
                                                    <b class="fn"><?php echo $ncomment['autor'];?></b>
                                                </div>
                                                <div class="comment-metadata">
                                                    <a href="index.html">
                                                        <time><?php echo $ncomment['date'];?></time>
                                                    </a>
                                                </div>
                                            </footer>
                                            <div class="comment-content">
                                                <p><?php echo $ncomment['text'];?></p>
                                            </div>
											<?php if($_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2" or $_SESSION['logged_user']->login == $ncomment['login']): ?>
											<div class="reply">
												<input type="submit" name="<?php echo $ncomment['id'];?>" class="btn" value="Удалить">
                                            </div>
											<?php endif; ?>
                                        </article>

<?php endforeach; ?> 


										
										<div id="addnewcomment">
										
										</div>
										
										
                                    </li>
                                </ol>
        <?php if ( isset ($_SESSION['logged_user']) ) : ?>
                                <div class="comment-respond">
                                    <h3 class="comment-reply-title">Добавить комментарий</h3><br>

                                    
                                        <p class="comment-form-comment">
                                            <label>Текст комментария</label>
                                            <textarea name="commenttext" id="commenttext" cols="45" placeholder="Ваш комментарий" rows="5" maxlength="299" required="required"></textarea>
                                        </p>
                                        
                                        <p class="form-submit">
                                            <input type="submit" name="entercomment" id="entercomment" class="submit" value="Отправить">
                                        </p>
                                    
                                </div>
        <?php else : ?>
                                <div class="comment-respond">
                                    <h3 class="comment-reply-title"><a href="login.php">Авторизуйтесь</a>, чтобы добавить комментарий</h3><br>  
                                </div>
        <?php endif; ?>								
								
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <aside class="widget-area">
						
						
						<?php if($_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2"):

						if ( isset($data['statgood']) ){
						$autor->status = "1";
						R::store($autor);		
						}
						if ( isset($data['statotkl']) ){
					    $autor->status = "0";
						R::store($autor);	
						}
						if ( isset($data['statdel']) ){
							$delstatfile = $autor->src;
							$delstatimg = $autor->imgsrc;
							$delstatcomments = R::getAll("DELETE FROM `comments` WHERE src ='$delstatfile';");
							unlink(__DIR__ . $delstatfile);
							unlink(__DIR__ . $delstatimg);
					         R::trash($autor);
						}
						?>
						<section class="widget widget_search">
                                <h3 class="widget-title">Модерирование</h3>

                                <form class="search-form" method="POST">
								<?php if($autor->status == "0"): ?>
                                    <p class="form-submit">
                                            <input type="submit" name="statgood" id="statgood" class="btn" value="Одобрить">
                                    </p>
								<?php endif; ?>	
									<p class="form-submit">
                                            <input type="submit" name="statotkl" id="statotkl" class="btn" value="Отклонить">
                                    </p>
									<?php if($_SESSION['logged_user']->groupuser == "3"): ?>	
									<p class="form-submit">
                                            <input type="submit" name="statdel" id="statdel" class="btn" value="Удалить">
                                    </p>	
						         <?php endif; ?>
                                </form>
                        </section>
						<?php endif;?>
						
						<?php if($_SESSION['logged_user']->login == $autor->owner): ?>
						<section class="widget widget_search">
                                <h3 class="widget-title">Управление статьей</h3>
								
								
								<?php 
								
								if ( isset($data['statdelowner']) ){
						        $autor->status = "3";
						        R::store($autor);		
						        }
								?>

                                <form class="search-form"  method="POST">
								<?php if($autor->status == "1" or $autor->status == "0"):?>
                                    <p class="form-submit">
                                            <input type="submit" name="statdelowner" id="statdelowner" class="btn" value="Удалить">
                                    </p>
                                <?php else: ?>
								<a>Статья удалена</a>
								<?php endif; ?>
								
                                </form>
                        </section>
						<?php endif; ?>
						
						
                            <section class="widget widget_search">
                                <h3 class="widget-title">Поиск</h3>

                                <form class="search-form">
                                    <label>
                                        <span class="screen-reader-text">Поиск</span>
                                        <input type="search" class="search-field" placeholder="Поиск...">
                                    </label>
                                    <button type="submit">
                                        <i class="flaticon-search"></i>
                                    </button>
                                </form>
                            </section>

                            <section class="widget widget_orgo_posts_thumb">
                                <h3 class="widget-title">Популярные обсуждения</h3>
								
								<?php 
								

                  foreach ($topblogs as $topblg): 
                 $hreff = 'index.php?blog=1&src=';
                  $hreff = $hreff . $topblg['src'];

								?>

                                <article class="item">
                                    <a href="<?php echo $hreff; ?>" class="thumb">
                                        <span><img width="80" height="80" src="<?php echo $topblg['imgsrc']; ?>" alt="image"></span>
                                    </a>
                                    <div class="info">
                                        <span><?php echo $topblg['date']; ?></span>
                                        <h4 class="title usmall"><a href="<?php echo $hreff; ?>"><?php echo $topblg['name']; ?></a></h4>
                                    </div>
                                </article>
								
								
                   <?php endforeach; ?> 
                                
                            </section>

                            <section class="widget widget_categories">
                                <h3 class="widget-title">Категории</h3>

                                <ul>
                                    <li><a href="index.php?allblogs=1&category=Растениеводство">Растениеводство</a></li>
                                    <li><a href="index.php?allblogs=1&category=Животноводство">Животноводство</a></li>
                                    <li><a href="index.php?allblogs=1&category=Птицеводство">Птицеводство</a></li>
                                    <li><a href="index.php?allblogs=1&category=Сельхоз Техника">Сельхоз Техника</a></li>
                                    <li><a href="index.php?allblogs=1&category=Объявления">Объявления</a></li>
                                    <li><a href="index.php?allblogs=1&category=Строительство">Строительство</a></li>
                                    <li><a href="index.php?allblogs=1&category=Рыбоводство">Рыбоводство</a></li>
									<li><a href="index.php?allblogs=1&category=Органическая Пища">Органическая Пища</a></li>
                                </ul>
                            </section>

                            <section class="widget widget_tag_cloud">
                                <h3 class="widget-title">Популярные теги</h3>

                                <div class="tagcloud">
                                    <a href="#">Полезно</a><br>
                                    <a href="#">Новые идеи</a><br>
                                    <a href="#">Органика</a><br>
                                    <a href="#">Чистые продукты</a><br>
                                    <a href="#">Химия в продуктах</a><br>
                                    <a href="#">Новые способы</a><br>
                                    <a href="#">Рецепты</a><br>
                                    <a href="#">Вкусно и полезно</a><br>
                                </div>
                            </section>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
	   <?php else : ?>

	   <?php if (isset($lk)) : ?>
	   
	   
	   <section class="blog-area pt-100 pb-70">
            <div class="container">
                <div class="section-title">
				
                    
					 <h2>Личный Кабинет</h2>
					 
                    <section class="contact-info-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bx-envelope'></i>
                            </div>

                            <h3>Email</h3>
                            <p><a href=""><?php echo substr($_SESSION['logged_user']->email,0,11)."..." ?></a></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bx-user'></i>
                            </div>

                            <h3>Логин</h3>
                            <p><?php echo $_SESSION['logged_user']->login; ?></p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bxs-key'></i>
                            </div>

                            <h3>Пароль</h3>
                            <p><a href="#svs">Поменять</a></p>
                        </div>
                    </div>
				
					<div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bxs-phone'></i>
                            </div>

                            <h3>Телефон</h3>
                            <p><a href=""><?php echo $_SESSION['logged_user']->phonenumber; ?></a></p>
                        </div>
                    </div>
					
					<div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bx-map'></i>
                            </div>
<a name="svs"></a>
                            <h3>Город</h3>
                            <p><a href=""><?php echo $_SESSION['logged_user']->city; ?></a></p>
                        </div>
                    </div>
					
					<div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="contact-info-box">
                            <div class="icon">
                                <i class='bx bx-user-check'></i>
                            </div>
<?php 
switch($_SESSION['logged_user']->groupuser){
	
	case 1: 
	$Ugrp = "Пользователь";
	break;
	
	case 2:
	$Ugrp = "Модератор";
	break;
	
	case 3:
	$Ugrp = "Админ";
	break;
}

 ?>
                            <h3>Группа</h3>
                            <p><a href=""><?php echo $Ugrp; ?></a></p>
                        </div>
                    </div>
				
                </div>
            </div>
        </section>
		
		
		<?php
if ( isset($data['smenapass']) ){
	$errors = array();
	if($_SESSION['logged_user']->changepassdate == Date(Ymd)){
		$errors[] = 'Вы сегодня уже меняли пароль!';
	}
	if($data['pass'] != $data['pass2']){
		$errors[] = 'Пароли не совпадают!';
	}
	if ( empty($errors) )
    {
	$_SESSION['logged_user']->password = password_hash($data['pass'], PASSWORD_DEFAULT);
	$_SESSION['logged_user']->changepassdate = Date(Ymd);
	R::store($_SESSION['logged_user']);
	}else{
		echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
	}
	
}
		?>
		
		
		<div class="contact-form">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Пароль*</label>
                                    <input type="password" name="pass" id="pass" class="form-control" value="<?php echo @$data['pass']; ?>" required data-error="Вы забыли ввести пароль!">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Повтор пароля*</label>
                                    <input type="password" name="pass2" id="pass2" class="form-control" value="<?php echo @$data['pass2']; ?>" required data-error="Вы забыли ввести повторный пароль!">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <button type="submit" name="smenapass" class="default-btn">Сменить</button>
                            </div>
                        </div>
                    </form>
        </div>
					
</div>
				
				
				<div class="section-title">
				
                    
					 <h2>Ваши Статьи</h2>
					 
                    <p></p>
                </div>
				
				
<?php 
$user = $_SESSION['logged_user']->login;
$individblogs = R::getAll("SELECT * FROM `blogs` WHERE owner ='$user' ORDER BY `vievs` DESC ;");
 foreach ($individblogs as $topblg): 
 $hreff = 'index.php?blog=1&src=';
$hreff = $hreff . $topblg['src'];
 ?>
<?php 

if (($schet % 3) == 0 or $schet == 0) {
 echo '<div class="row">';
}
$schet=$schet + 1;
?>
                   
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <div class="image">
                                <a href="<?php echo $hreff; ?>"><img width="650" height="500" src="<?php echo $topblg['imgsrc']; ?>" alt="image"></a>
                                
                                <div class="date">
                                    <span><?php echo $topblg['date']; ?></span>
                                </div>
                            </div>
                            <div class="content">
                                <h3>
                                    <a href="<?php echo $hreff; ?>"><?php echo $topblg['name']; ?></a>
                                </h3>
                                <a href="<?php echo $hreff; ?>" class="blog-btn">Читать далее +</a>
                            </div>
                        </div>
                    </div>  




<?php 
if (($schet % 3) == 0 or $schet == 0) {
 echo '</div>';
}
?>
			
					                 
<?php endforeach; ?>  
				
				
		    </div>
		</section>		
	   
	   
	   <?php else : ?>
	   
	   <?php if (isset($allblogs)) : ?>
	   
	   
        <section class="blog-area pt-100 pb-70">
            <div class="container">
                <div class="section-title">
				<?php if (isset($selectetcateg)) : ?>
                    <h2><?php echo $selectetcateg; ?></h2>
					 <?php else : ?>
					<?php if(isset($moderatestat) and $_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2"): ?>
					 <h2>На модерации</h2>
					 <?php else: ?>
					 <h2>Все статьи</h2>
					 <?php endif; ?>
					 <?php endif; ?>
                    <p></p>
                </div>
				
	<?php if (isset($selectetcateg)) : ?>	






<?php 
$categblogs = R::getAll("SELECT * FROM `blogs` WHERE category = '$selectetcateg' AND status = '1' ORDER BY `vievs` DESC ;");
 foreach ($categblogs as $topblg): 
 $hreff = 'index.php?blog=1&src=';
$hreff = $hreff . $topblg['src'];
 ?>
<?php 

if (($schet % 3) == 0 or $schet == 0) {
 echo '<div class="row">';
}
$schet=$schet + 1;
?>
                   
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <div class="image">
                                <a href="<?php echo $hreff; ?>"><img width="650" height="500" src="<?php echo $topblg['imgsrc']; ?>" alt="image"></a>
                                
                                <div class="date">
                                    <span><?php echo $topblg['date']; ?></span>
                                </div>
                            </div>
                            <div class="content">
                                <h3>
                                    <a href="<?php echo $hreff; ?>"><?php echo $topblg['name']; ?></a>
                                </h3>
                                <a href="<?php echo $hreff; ?>" class="blog-btn">Читать далее +</a>
                            </div>
                        </div>
                    </div>  




<?php 
if (($schet % 3) == 0 or $schet == 0) {
 echo '</div>';
}
?>
			
					                 
<?php endforeach; ?>  	





	
				
	<?php else : ?>		
				
<?php 
if($moderatestat == "1" and $_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2"):	
$topblogs = R::getAll("SELECT * FROM `blogs` WHERE status = '0' ORDER BY `vievs` DESC ;");
endif;
if($moderatestat == "2" and $_SESSION['logged_user']->groupuser == "3" or $_SESSION['logged_user']->groupuser == "2"):	
$topblogs = R::getAll("SELECT * FROM `blogs` ORDER BY `vievs` DESC ;");
endif;

foreach ($topblogs as $topblg): 
$hreff = 'index.php?blog=1&src=';
$hreff = $hreff . $topblg['src'];

 ?>
<?php 

if (($schet % 3) == 0 or $schet == 0) {
 echo '<div class="row">';
}
$schet=$schet + 1;
?>
                   
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <div class="image">
                                <a href="<?php echo $hreff; ?>"><img width="650" height="500" src="<?php echo $topblg['imgsrc']; ?>" alt="image"></a>
                                
                                <div class="date">
                                    <span><?php echo $topblg['date']; ?></span>
                                </div>
                            </div>
                            <div class="content">
                                <h3>
                                    <a href="<?php echo $hreff; ?>"><?php echo $topblg['name']; ?></a>
                                </h3>
                                <a href="<?php echo $hreff; ?>" class="blog-btn">Читать далее +</a>
                            </div>
                        </div>
                    </div>  




<?php 
if (($schet % 3) == 0 or $schet == 0) {
 echo '</div>';
}
?>
			
					                 
<?php endforeach; ?>  	
				

			   
			   <?php endif; ?>
			   
                </div
            </div>
</section>
	   
	   <?php else : ?>
	   
	   

        <!-- Start Main Slider Area -->
        <div class="main-slider-area">
            <div class="home-slides owl-carousel owl-theme">

			<?php if ( !isset ($_SESSION['logged_user']) ) : ?>
			

                <div class="main-slider-item main-slider-two">
                    <div class="d-table">
                        <div class="d-table-cell">
                            <div class="container-fluid">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                        <div class="main-slider-content">
                                            
                                            <h1>Органическая пища - залог здоровья</h1>
                                            <p>Органические пищевые продукты – единственный способ получить нужные для здоровья человека витамины, белки, углеводы и минеральные соли</p>
                                            <div class="slider-btn">
                                                <a href="login.php" class="default-btn">Войти</a>
                                                <a href="register.php" class="optional-btn">Зарегистрироваться</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="main-slider-image-wrap">
										
										
										
                                            <img src="assets/img/main-slider/organic.jpg" alt="image">
										

                                            <div class="slider-shape">
                                                <img src="assets/img/main-slider/main-slider-shape.png" alt="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
<?php else : ?>



                <div class="main-slider-item main-slider-two">
                    <div class="d-table">
                        <div class="d-table-cell">
                            <div class="container-fluid">
                                <div class="row align-items-center">
                                    <div class="col-lg-6">
                                        <div class="main-slider-content">
                                            
                                            <h1>Разместите статью</h1>
                                            <p>Разместив новую статью у нас на сайте, вы можете получить комментарии от аграриев</p>
                                            <div class="slider-btn">
                                                <a href="write.php" class="optional-btn">Создать статью</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="main-slider-image-wrap">
                                            <img src="assets/img/main-slider/organic.jpg" alt="image">

                                            <div class="slider-shape">
                                                <img src="assets/img/main-slider/main-slider-shape.png" alt="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php endif; ?>
            </div>

            
        </div>
        <!-- End Main Slider Area -->

        <!-- Start katalog -->
		
        <section class="services-area bg-image pt-100 pb-70">
            <div class="container">
                <div class="section-title">
                    <h2>Категории</h2>
                    <p>Выберите интересующую вас категорию</p>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div name="Растениеводство" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-apple"></i>
                            </div>
                            <h3>Растениеводство</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Растениеводство" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Животноводство" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-nature"></i>
                            </div>
                            <h3>Животноводство</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Животноводство" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Птицеводство" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-forest"></i>
                            </div>
                            <h3>Птицеводство</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Птицеводство" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Сельхоз Техника" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-organic"></i>
                            </div>
                            <h3>Сельхоз Техника</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Сельхоз Техника" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Объявления" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-plant"></i>
                            </div>
                            <h3>Объявления</h3>
                            <p></p>
                            <a href="" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Строительство" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-leaf"></i>
                            </div>
                            <h3>Строительство</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Строительство" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Рыбоводство" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-heart"></i>
                            </div>
                            <h3>Рыбоводство</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Рыбоводство" class="read-btn">Выбрать +</a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div name="Органическая пища" class="single-services-box">
                            <div class="icon">
                                <i class="flaticon-apple"></i>
                            </div>
                            <h3>Органическая пища</h3>
                            <p></p>
                            <a href="index.php?allblogs=1&category=Органическая пища" class="read-btn">Выбрать +</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End katalog -->

        <!-- Start blogs -->
		

        <section class="blog-area pt-100 pb-70">
            <div class="container">
                <div class="section-title">
                    <h2>Популярные статьи</h2>
                    <p></p>
                </div>
				<div class="row">
				
<?php 
$topblogs = R::getAll("SELECT * FROM `blogs` WHERE status = '1' ORDER BY `vievs` DESC limit 3 ;");


 foreach ($topblogs as $topblg): 
 $hreff = 'index.php?blog=1&src=';
$hreff = $hreff . $topblg['src'];
 ?>

                   
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <div class="image">
                                <a href="<?php echo $hreff; ?>"><img width="650" height="500" src="<?php echo $topblg['imgsrc']; ?>" alt="image"></a>
                                
                                <div class="date">
                                    <span><?php echo $topblg['date']; ?></span>
                                </div>
                            </div>
                            <div class="content">
                                <h3>
                                    <a href="<?php echo $hreff; ?>"><?php echo $topblg['name']; ?></a>
                                </h3>
                                <a href="<?php echo $hreff; ?>" class="blog-btn">Читать далее +</a>
                            </div>
                        </div>
                    </div>            
                   
<?php endforeach; ?>  	
				
				

               
                </div
            </div>
        </section>
        <!-- End blog -->

       
    

       

        <!-- Start Deal Area -->
        <section class="deal-area ptb-100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="deal-title">
                            <h2>Загляните в наш магазин</h2>
                            <p>Мы подобрали для вас самые органические и здоровые продукты, выращенные в экологически чистой почве</p>
                        </div>

                        <div class="deal-content">
                            <div id="timer" class="flex-wrap d-flex justify-content-center">
                                <div id="days" class="align-items-center flex-column d-flex justify-content-center"></div>
                                <div id="hours" class="align-items-center flex-column d-flex justify-content-center"></div>
                                <div id="minutes" class="align-items-center flex-column d-flex justify-content-center"></div>
                                <div id="seconds" class="align-items-center flex-column d-flex justify-content-center"></div>
                            </div>

                            <div class="deal-btn">
                                <a href="#" class="default-btn">В магазин</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="deal-image">
                            <img src="assets/img/deal/deal-1.png" alt="image">

                            <div class="offer">
							<span>Скидки</span><br><br>
                                <h4>50%</h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="deal-shape">
                <div class="shape-1">
                    <img src="assets/img/deal/deal-shape-1.png" alt="image">
                </div>
                <div class="shape-2">
                    <img src="assets/img/deal/deal-shape-2.png" alt="image">
                </div>
                <div class="shape-3">
                    <img src="assets/img/deal/deal-shape-3.png" alt="image">
                </div>
                <div class="shape-4">
                    <img src="assets/img/deal/deal-shape-4.png" alt="image">
                </div>
            </div>
        </section>
        <!-- End Deal Area -->

        

        <!-- Start Gallery Area -->
        <section class="gallery-area bg-fafafa pt-100 pb-70">
            <div class="container-fluid">
                <div class="section-title">
                    <h2>Галерея наших продуктов</h2>
                    
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="single-gallery-box">
                            <div class="gallery-image">
                                <a href="#"><img src="assets/img/gallery/glr-1.jpg" alt="image"></a>
                            </div>

                            <div class="gallery-content">
                                <h3>
                                    <a href="shop-details.html">Томаты</a>
                                </h3>
                                <span></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="single-gallery-box">
                            <div class="gallery-image">
                                <a href="#"><img src="assets/img/gallery/glr-2.jpg" alt="image"></a>
                            </div>

                            <div class="gallery-content">
                                <h3>
                                    <a href="#">Яйца</a>
                                </h3>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single-gallery-box">
                            <div class="gallery-image">
                                <a href="#"><img src="assets/img/gallery/glr-3.jpg" alt="image"></a>
                            </div>

                            <div class="gallery-content">
                                <h3>
                                    <a href="#">Огурцы</a>
                                </h3>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single-gallery-box">
                            <div class="gallery-image">
                                <a href="#"><img src="assets/img/gallery/glr-4.jpg" alt="image"></a>
                            </div>

                            <div class="gallery-content">
                                <h3>
                                    <a href="#">Красный лук</a>
                                </h3>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single-gallery-box">
                            <div class="gallery-image">
                                <a href="#"><img src="assets/img/gallery/glr-5.jpg" alt="image"></a>
                            </div>

                            <div class="gallery-content">
                                <h3>
                                    <a href="#">Картофель</a>
                                </h3>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Gallery Area -->
		<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
        <!-- Start Footer Area -->
        <div class="footer-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <div class="logo">
                                <h2>
                                    <a href="index.php">SelhozDom</a>
                                </h2>
                            </div>
                            <p>Мы в социальных сетях</p>
                            <ul class="social">
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <i class='bx bxl-facebook'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="twitter" target="_blank">
                                        <i class='bx bxl-twitter'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="pinterest" target="_blank">
                                        <i class='bx bxl-pinterest-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="linkedin" target="_blank">
                                        <i class='bx bxl-linkedin'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h3>Instagram</h3>

                            <ul class="instagram-list">
                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram1.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram2.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram3.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram4.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram5.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram6.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram7.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram8.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>

                                <li>
                                    <div class="box">
                                        <img src="assets/img/instagram/instagram1.jpg" alt="image">
                                        <i class="bx bxl-instagram"></i>
                                        <a href="#" target="_blank" class="link-btn"></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget pl-5">
                            <h3>Быстрое меню</h3>
    
                            <ul class="quick-links">
                                <li>
                                    <a href="#">Главная</a>
                                </li>
                                <li>
                                    <a href="#">Обсуждения</a>
                                </li>
                                <li>
                                    <a href="#">Магазин</a>
                                </li>
                                <li>
                                    <a href="#">Категории</a>
                                </li>
                                <li>
                                    <a href="#">Контакты</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h3>Контакты</h3>

                            <ul class="footer-contact-info">
                                <li>
                                    <i class='bx bxs-phone'></i>
                                    <span>Телефон</span>
                                    <a href="tel:407409202288">(988)54 764 39</a>
                                </li>
                                <li>
                                    <i class='bx bx-envelope'></i>
                                    <span>Email</span>
                                    <a href="mailto:vaddd.orov@gmail.com">v.o@gmail.com</a>
                                </li>
                                <li>
                                    <i class='bx bx-map'></i>
                                    <span>Адресс</span>
                                    Москва
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Area -->

        <!-- Start Copy Right Area -->
        <div class="copyright-area">
            <div class="container">
                <div class="copyright-area-content">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <p>
                                <i class="far fa-copyright"></i> 
                                Все права защищены @2021 SelhozDom.
                                
                            </p>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <ul>
                                <li>
                                    <a href="">Политика приватности</a>
                                </li>
                                <li>
                                    <a href="">Другие документы</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Copy Right Area -->

        <!-- Start Go Top Area -->
        <div class="go-top">
            <i class='bx bx-up-arrow-alt'></i>
        </div>
        <!-- End Go Top Area -->

       

        <!-- Jquery Slim JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Meanmenu JS -->
        <script src="assets/js/jquery.meanmenu.js"></script>
        <!-- Nice Select JS -->
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <!-- Owl Carousel JS -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Magnific Popup JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- Odometer JS -->
        <script src="assets/js/odometer.min.js"></script>
        <!-- Jquery Appear JS -->
        <script src="assets/js/jquery.appear.min.js"></script>
        <!-- Ajaxchimp JS -->
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<!-- Form Validator JS -->
		<script src="assets/js/form-validator.min.js"></script>
		<!-- Contact JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- Wow JS -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Custom JS -->
        <script src="assets/js/main.js"></script>
		
		<!-- Comments -->
		<script src="assets/js/comments.js"></script>
    </body>
</html>